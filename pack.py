import shutil
from os.path import exists
import os

file_exists = exists('game.zip')
if file_exists:
    os.remove('game.zip')

shutil.make_archive('game', 'zip', 'desktop/build/jpackage/desktop')