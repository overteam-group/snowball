package club.anims.snowball.utils;

import com.badlogic.gdx.graphics.Color;

public class ColorUtil {
    public static Color fromHex(String hex) {
        hex = hex.replaceAll("[^0-9A-Fa-f]", "");
        var r = Integer.parseInt(hex.substring(0, 2), 16);
        var g = Integer.parseInt(hex.substring(2, 4), 16);
        var b = Integer.parseInt(hex.substring(4, 6), 16);
        return new Color(r / 255f, g / 255f, b / 255f, 1f);
    }

    public static String toHex(Color color) {
        var r = (int) (color.r * 255);
        var g = (int) (color.g * 255);
        var b = (int) (color.b * 255);
        return String.format("%02x%02x%02x", r, g, b);
    }
}
