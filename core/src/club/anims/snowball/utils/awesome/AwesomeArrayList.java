package club.anims.snowball.utils.awesome;

import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;

@NoArgsConstructor
public class AwesomeArrayList<T> extends ArrayList<T> {
    public AwesomeArrayList(ArrayList<T> arrayList) {
        addAll(arrayList);
    }

    @SafeVarargs
    public AwesomeArrayList(T... elements) {
        super(Arrays.asList(elements));
    }

    @SafeVarargs
    public final AwesomeArrayList<T> awesomeAdd(T... elements) {
        addAll(Arrays.asList(elements));
        return this;
    }
}
