package club.anims.snowball.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class ShapeRendererUtil {
    public static void drawRect(ShapeRenderer shapeRenderer, float x, float y, float width, float height, Color color) {
        shapeRenderer.setColor(color);
        shapeRenderer.rect(x, y, width, height);
    }
}
