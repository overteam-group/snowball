package club.anims.snowball.utils;

import club.anims.snowball.entities.RectangleEntity;

public class EntityUtil {
    public static boolean isColliding(RectangleEntity a, RectangleEntity b) {
        return a.getX() < b.getX() + b.getWidth() && a.getX() + a.getWidth() > b.getX() && a.getY() < b.getY() + b.getHeight() && a.getY() + a.getHeight() > b.getY();
    }
}
