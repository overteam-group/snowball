package club.anims.snowball.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class GdxUtil {
    public static final Color DEFAULT_COLOR = ColorUtil.fromHex("#020308");

    public static void setDefaultColor(){
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(DEFAULT_COLOR.r, DEFAULT_COLOR.g, DEFAULT_COLOR.b, DEFAULT_COLOR.a);
    }

    public static Skin getSkin() {
        return new Skin(Gdx.files.internal("skin/tracer-ui.json"));
    }
}
