package club.anims.snowball;

import club.anims.snowball.networking.SnowballClient;
import club.anims.snowball.screens.MainMenuScreen;
import com.badlogic.gdx.Game;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Snowball extends Game {
	public static Snowball instance;

	private SnowballClient client;

	public Snowball() {
		instance = this;
	}
	
	@Override
	public void create () {
		this.setScreen(new MainMenuScreen());
	}
}
