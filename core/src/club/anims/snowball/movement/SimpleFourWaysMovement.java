package club.anims.snowball.movement;

public interface SimpleFourWaysMovement extends MovementAdapter {
    @Override
    default void updateMovement() {
        moveLeft();
        moveRight();
        moveUp();
        moveDown();
    }

    void moveLeft();
    void moveRight();
    void moveUp();
    void moveDown();
}