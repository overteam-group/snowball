package club.anims.snowball.movement;

public interface MovementAdapter {
    void updateMovement();
}
