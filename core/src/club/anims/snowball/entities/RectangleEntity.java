package club.anims.snowball.entities;

import club.anims.snowball.utils.ShapeRendererUtil;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import lombok.*;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor @Builder
public class RectangleEntity implements Entity{
    private float x;
    private float y;
    private float width;
    private float height;
    private Color color;
    private boolean isCollidable;

    private ShapeRenderer shapeRenderer;

    @Override
    public void render() {
        if(shapeRenderer == null) return;
        ShapeRendererUtil.drawRect(shapeRenderer, x, y, width, height, color);
    }

    @Override
    public void onCollision(Entity obstacle) {

    }
}
