package club.anims.snowball.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class StageRectangleEntityAdapter extends RectangleEntity {
    protected final Stage localStage;

    public StageRectangleEntityAdapter(Stage localStage, boolean autoInit) {
        this.localStage = localStage;
        if(autoInit) init();
    }

    public StageRectangleEntityAdapter addActor(Actor actor) {
        localStage.addActor(actor);
        return this;
    }

    public void afterInit(){}

    @Override
    public void render() {
        localStage.act();
        localStage.draw();
    }

    public void init() {
        if(localStage == null){
            throw new RuntimeException("Stage is null");
        }
        Gdx.input.setInputProcessor(localStage);
        afterInit();
    }
}