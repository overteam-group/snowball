package club.anims.snowball.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ButtonEntity extends RectangleEntity implements ClickableEntity {
    private String text;
    private Color textColor;
    private BitmapFont font;
    private SpriteBatch spriteBatch;

    public ButtonEntity(float x, float y, float width, float height, String text, Color textColor, BitmapFont font, Color buttonColor, SpriteBatch spriteBatch, ShapeRenderer shapeRenderer) {
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
        setText(text);
        setTextColor(textColor);
        setFont(font);
        setColor(buttonColor);
        setSpriteBatch(spriteBatch);
        setShapeRenderer(shapeRenderer);
    }

    public ButtonEntity(float x, float y, float width, float height, String text, Color textColor, Color buttonColor, SpriteBatch spriteBatch, ShapeRenderer shapeRenderer) {
        this(x, y, width, height, text, textColor, null, buttonColor, spriteBatch, shapeRenderer);
        var newFont = new BitmapFont();
        newFont.setColor(textColor);
        setFont(newFont);
    }

    @Override
    public void render() {
        super.render();
        if(spriteBatch == null) return;
        font.draw(spriteBatch, text, getX()+getWidth()/8, getY()+getHeight()/2);
    }

    @Override
    public void onClick() {

    }
}
