package club.anims.snowball.entities;

public interface Entity {
    void render();
    void onCollision(Entity obstacle);
}
