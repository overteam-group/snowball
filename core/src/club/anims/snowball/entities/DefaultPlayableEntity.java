package club.anims.snowball.entities;

import club.anims.snowball.movement.SimpleFourWaysMovement;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class DefaultPlayableEntity extends RectangleEntity implements SimpleFourWaysMovement {
    private static float DEFAULT_SPEED = 1.0f;
    private float speed = DEFAULT_SPEED;

    public DefaultPlayableEntity(float x, float y, float width, float height, Color color, boolean isCollidable, float speed, ShapeRenderer shapeRenderer) {
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
        setColor(color);
        setCollidable(isCollidable);
        setSpeed(speed);
        setShapeRenderer(shapeRenderer);
    }

    public DefaultPlayableEntity(float x, float y, float width, float height, Color color, boolean isCollidable, ShapeRenderer shapeRenderer) {
        this(x, y, width, height, color, isCollidable, DEFAULT_SPEED, shapeRenderer);
    }

    public DefaultPlayableEntity(float x, float y, float width, float height, Color color, ShapeRenderer shapeRenderer) {
        this(x, y, width, height, color, true, DEFAULT_SPEED, shapeRenderer);
    }

    @Override
    public void moveLeft() {
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            setX(getX() - speed);
        }
    }

    @Override
    public void moveRight() {
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            setX(getX() + speed);
        }
    }

    @Override
    public void moveUp() {
        if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
            setY(getY() + speed);
        }
    }

    @Override
    public void moveDown() {
        if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            setY(getY() - speed);
        }
    }
}
