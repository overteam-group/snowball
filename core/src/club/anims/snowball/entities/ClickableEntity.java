package club.anims.snowball.entities;

public interface ClickableEntity {
    void onClick();
}