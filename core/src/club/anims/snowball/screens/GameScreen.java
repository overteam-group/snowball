package club.anims.snowball.screens;

import club.anims.snowball.Snowball;
import club.anims.snowball.entities.DefaultPlayableEntity;
import club.anims.snowball.entities.RectangleEntity;
import club.anims.snowball.utils.GdxUtil;
import club.anims.snowball.utils.awesome.AwesomeArrayList;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class GameScreen extends ScreenAdapter {
    private OrthographicCamera camera;
    private ShapeRenderer shapeRenderer;

    private AwesomeArrayList<RectangleEntity> entities;
    private DefaultPlayableEntity player;

    public GameScreen(DefaultPlayableEntity player) {
        //Setup
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, 0);
        shapeRenderer = new ShapeRenderer();
        this.player = player;
        this.player.setShapeRenderer(shapeRenderer);
        //End setup
        entities = new AwesomeArrayList<>(
                this.player
        );
    }

    public void update(float delta) {
        entities = new AwesomeArrayList<>(Snowball.instance.getClient().fetch());
        entities.forEach(entity -> entity.setShapeRenderer(shapeRenderer));
        player.updateMovement();
        Snowball.instance.getClient().update(player);
        entities.add(player);
//        entities.stream().filter(entity -> entity instanceof DefaultPlayableEntity).forEach(entity -> ((DefaultPlayableEntity) entity).updateMovement());
    }

    @Override
    public void render(float delta) {
        GdxUtil.setDefaultColor();

        update(delta);
        camera.update();

        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        entities.forEach(RectangleEntity::render);

        shapeRenderer.end();
    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
    }
}
