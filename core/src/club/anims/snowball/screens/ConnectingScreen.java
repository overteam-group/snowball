package club.anims.snowball.screens;

import club.anims.snowball.Snowball;
import club.anims.snowball.entities.DefaultPlayableEntity;
import club.anims.snowball.entities.StageRectangleEntityAdapter;
import club.anims.snowball.networking.Port;
import club.anims.snowball.networking.SnowballClient;
import club.anims.snowball.utils.GdxUtil;
import club.anims.snowball.utils.ShapeRendererUtil;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static club.anims.snowball.utils.ColorUtil.fromHex;

public class ConnectingScreen extends ScreenAdapter {
    public record ConnectingScreenConfig(String ip, String name, DefaultPlayableEntity player) {
    }

    private static final Color DEFAULT_BUTTON_COLOR = fromHex("#ffffff");
    private static final Color DEFAULT_BUTTON_TEXT_COLOR = fromHex("#ed0514");
    private ShapeRenderer shapeRenderer;
    private Stage stage;
    private StageRectangleEntityAdapter stageRectangleEntityAdapter;
    private final Logger LOGGER = LoggerFactory.getLogger(ConnectingScreen.class);
    private ConnectingScreenConfig config;
    private boolean ready;

    public ConnectingScreen(ConnectingScreenConfig config) {
        shapeRenderer = new ShapeRenderer();
        stage = new Stage();
        this.config = config;
        stageRectangleEntityAdapter = new StageRectangleEntityAdapter(stage, true){
            @Override
            public void afterInit() {
                var connectionInfo = new TextField("Connecting to " + config.ip, GdxUtil.getSkin());
                connectionInfo.setDisabled(true);
                connectionInfo.setWidth(Gdx.graphics.getWidth()/2);
                connectionInfo.setPosition(Gdx.graphics.getWidth()/2-connectionInfo.getWidth()/2, Gdx.graphics.getHeight()/2);
                addActor(connectionInfo);
            }
        };
        establishConnection();
    }

    public void establishConnection() {
        if(Snowball.instance.getClient()==null){
            var hostnameAndIp = config.ip.split(":");
            if(hostnameAndIp.length<2){
                LOGGER.error("Invalid ip: " + config.ip);
                Snowball.instance.setScreen(new MainMenuScreen());
                return;
            }
            Port port;
            try{
                port = new Port(hostnameAndIp[1]);
            }catch (Exception e){
                LOGGER.error("Invalid port: " + hostnameAndIp[1]);
                Snowball.instance.setScreen(new MainMenuScreen());
                return;
            }
            Snowball.instance.setClient(new SnowballClient(hostnameAndIp[0], port.get()));
        }
        Snowball.instance.getClient().begin(config.player, config.name);
        ready = true;
    }

    public void update(float delta) {
        if(ready){
            Snowball.instance.setScreen(new GameScreen(config.player));
        }
    }

    @Override
    public void render(float delta) {
        update(delta);
        GdxUtil.setDefaultColor();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        ShapeRendererUtil.drawRect(shapeRenderer, 1, 1, Gdx.graphics.getWidth()-1, Gdx.graphics.getHeight()-5, DEFAULT_BUTTON_TEXT_COLOR);
        shapeRenderer.end();
        stageRectangleEntityAdapter.render();
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        shapeRenderer.dispose();
    }
}
