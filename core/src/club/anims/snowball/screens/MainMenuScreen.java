package club.anims.snowball.screens;

import club.anims.snowball.Snowball;
import club.anims.snowball.entities.ClickableEntity;
import club.anims.snowball.entities.DefaultPlayableEntity;
import club.anims.snowball.entities.RectangleEntity;
import club.anims.snowball.entities.StageRectangleEntityAdapter;
import club.anims.snowball.utils.ColorUtil;
import club.anims.snowball.utils.GdxUtil;
import club.anims.snowball.utils.awesome.AwesomeArrayList;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class MainMenuScreen extends ScreenAdapter {
    private static final float BUTTON_X = Gdx.graphics.getWidth()/4+Gdx.graphics.getWidth()/8;
    private static final float BUTTON_WIDTH = Gdx.graphics.getWidth()/4;

    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;
    private Stage stage;
    private AwesomeArrayList<RectangleEntity> entities;
    private DefaultPlayableEntity player;

    public MainMenuScreen() {
        shapeRenderer = new ShapeRenderer();
        spriteBatch = new SpriteBatch();
        stage = new Stage();
        player = new DefaultPlayableEntity(0, 0, 100, 100, Color.RED, true, 5f, null);
        entities = new AwesomeArrayList<>(
                new StageRectangleEntityAdapter(stage, true){
                    @Override
                    public void afterInit() {
                        var startGameButton = new TextButton("Start Game", GdxUtil.getSkin());
                        startGameButton.setPosition(BUTTON_X, Gdx.graphics.getHeight()/2+Gdx.graphics.getHeight()/16);
                        startGameButton.setWidth(BUTTON_WIDTH);

                        var exitButton = new TextButton("Exit", GdxUtil.getSkin());
                        exitButton.setPosition(BUTTON_X, Gdx.graphics.getHeight()/2-Gdx.graphics.getHeight()/8);
                        exitButton.setWidth(BUTTON_WIDTH);

                        var ipAddressTextField = new TextField("localhost:6969", GdxUtil.getSkin());
                        ipAddressTextField.setPosition(BUTTON_X, Gdx.graphics.getHeight()/2-Gdx.graphics.getHeight()/32);
                        ipAddressTextField.setWidth(BUTTON_WIDTH);

                        var colorHexTextField = new TextField("#ff0000", GdxUtil.getSkin());
                        colorHexTextField.setPosition(BUTTON_X, Gdx.graphics.getHeight()/2+Gdx.graphics.getHeight()/8);
                        colorHexTextField.setWidth(BUTTON_WIDTH);

                        var nameTextField = new TextField("Player", GdxUtil.getSkin());
                        nameTextField.setPosition(BUTTON_X, Gdx.graphics.getHeight()/2+Gdx.graphics.getHeight()/4);
                        nameTextField.setWidth(BUTTON_WIDTH);

                        startGameButton.addListener(new ClickListener(){
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                player.setColor(ColorUtil.fromHex(colorHexTextField.getText()));
                                Snowball.instance.setScreen(new ConnectingScreen(new ConnectingScreen.ConnectingScreenConfig(ipAddressTextField.getText(), nameTextField.getText(), player)));
                            }
                        });

                        exitButton.addListener(new ClickListener(){
                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                System.exit(0);
                            }
                        });

                        addActor(startGameButton);
                        addActor(exitButton);
                        addActor(ipAddressTextField);
                        addActor(colorHexTextField);
                        addActor(nameTextField);
                    }
                }
        );
    }

    public void detectClick(){
        if(!Gdx.input.isButtonJustPressed(0)) return;
        entities.stream().filter(entity -> entity instanceof ClickableEntity).forEach(entity -> {
            if(
                    Gdx.input.getX() < entity.getX()+entity.getWidth()
                    && Gdx.input.getX() > entity.getX()
                    && Gdx.graphics.getHeight()-Gdx.input.getY() < entity.getY()+entity.getHeight()
                    && Gdx.graphics.getHeight()-Gdx.input.getY() > entity.getY()
            ){
                ((ClickableEntity) entity).onClick();
            }
        });
    }

    public void update(float delta) {
        detectClick();
    }

    @Override
    public void render(float delta) {
        GdxUtil.setDefaultColor();
        update(delta);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        spriteBatch.begin();

        entities.forEach(RectangleEntity::render);

        spriteBatch.end();
        shapeRenderer.end();
    }

    @Override
    public void dispose() {
        super.dispose();
        shapeRenderer.dispose();
        spriteBatch.dispose();
    }
}
