package club.anims.snowball.networking;

import club.anims.snowball.entities.DefaultPlayableEntity;
import club.anims.snowball.entities.RectangleEntity;
import club.anims.snowball.networking.entities.PlayerEntity;
import club.anims.snowball.networking.requests.BeginRequest;
import club.anims.snowball.networking.requests.DefaultRequest;
import club.anims.snowball.networking.requests.UpdateRequest;
import club.anims.snowball.utils.ColorUtil;
import com.google.gson.Gson;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

@Getter
public class SnowballClient{
    public static SnowballClient clientInstance;

    private SocketData socketData;
    private String hostname;
    private int port;
    private boolean isConnectionEstablished;
    private final Logger LOGGER = LoggerFactory.getLogger(SnowballClient.class);
    private final Gson GSON = new Gson();

    public SnowballClient(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
        this.socketData = new SocketData();
        clientInstance = this;
    }

    public void begin(DefaultPlayableEntity player, String name) {
        if(!isConnectionEstablished){
            try{
                socketData.setSocket(new Socket(hostname, port));
                socketData.setOut(new PrintWriter(socketData.getSocket().getOutputStream(), true));
                socketData.setIn(new BufferedReader(new InputStreamReader(socketData.getSocket().getInputStream())));
                isConnectionEstablished = true;
                LOGGER.info("Connection established with {}:{}", hostname, port);
            }catch (Exception e){
                LOGGER.error(e.getMessage());
            }
        }
        var request = new DefaultRequest("begin", GSON.toJson(new BeginRequest(name, player.getX(), player.getY(), player.getWidth(), player.getHeight(), ColorUtil.toHex(player.getColor()))));
        socketData.getOut().println(GSON.toJson(request));
        LOGGER.info("begin request sent awaiting response");
        try{
            var response = socketData.getIn().readLine();
            if(response.equals("connection established: player successfully connected")){
                LOGGER.info("Player connected");
            }else{
                LOGGER.error("Player connection failed");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }

    public ArrayList<RectangleEntity> fetch(){
        var output = new ArrayList<RectangleEntity>();
        var request = new DefaultRequest("fetch", "");
        socketData.getOut().println(GSON.toJson(request));
        try{
            var response = socketData.getIn().readLine();
            var entities = GSON.fromJson(response, PlayerEntity[].class);
            Arrays.stream(entities).map(entity -> new RectangleEntity(entity.getX(), entity.getY(), entity.getWidth(), entity.getHeight(), ColorUtil.fromHex(entity.getColor()), false, null)).forEach(output::add);
        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }
        return output;
    }

    public void update(DefaultPlayableEntity player){
        var request = new DefaultRequest("update", GSON.toJson(new UpdateRequest(player.getX(), player.getY())));
        socketData.getOut().println(GSON.toJson(request));
        try{
            var response = socketData.getIn().readLine();
            if(response.equals("player updated"))
                LOGGER.debug("Player updated");
            else
                LOGGER.error("Player update failed {}", response);
        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }
}
