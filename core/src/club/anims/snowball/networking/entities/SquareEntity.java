package club.anims.snowball.networking.entities;

import lombok.*;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class SquareEntity {
    private float x;
    private float y;
    private float width;
    private float height;
    private String color;
}
