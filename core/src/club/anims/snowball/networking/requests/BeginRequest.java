package club.anims.snowball.networking.requests;

import lombok.*;

@AllArgsConstructor @NoArgsConstructor @Builder @Getter @Setter
public class BeginRequest {
    private String name;
    private float x;
    private float y;
    private float width;
    private float height;
    private String color;
}
