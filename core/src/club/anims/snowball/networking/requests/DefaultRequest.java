package club.anims.snowball.networking.requests;

import lombok.*;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor
public class DefaultRequest {
    private String command;
    private String data;
}
