package club.anims.snowball.networking.requests;

import lombok.*;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor
public class UpdateRequest {
    private float x;
    private float y;
}
