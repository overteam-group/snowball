package club.anims.snowball.networking;


public class Port {
    private int port;
    public Port(String value) {
        try{
            port = Integer.parseInt(value);
        }catch (NumberFormatException e){
            throwException();
        }
        if(port > 65535 || port < 0){
            throwException();
        }
    }

    public int get(){
        return port;
    }

    public void set(String value){
        this.port = new Port(value).get();
    }

    private void throwException(){
        throw new IllegalArgumentException("Port number must be between 0 and 65535");
    }
}
